# Ma178 Prosjekt

## Funksjoner

Funksjonene ligger i `functions.py` og kan bli importert i notebooks.

## Notebook

Kun skribling og testing akkurat nå.

## Utregning

Utregning ligger i `regning.xopp`. Filtypen tilhører notatprogrammet Xournal++. `regning.pdf` er denne filen eksportert til PDF format. Det er ikke 100% sikkert at jeg husker å reeksportere mellom hver commit, så den kan være utdatert.