import functions as func
import numpy as np

func.plotE(func.f_1dx, func.f_1, 0.0, 2, 0.00014283673760891302 , "$f_1(x)$ feil i intervallet [0,2]")
func.plotE(func.f_2dx, func.f_2, 0, 2*np.pi,0.002824858757062147, "$f_2(x)$ feil i intervallet [0,2$\pi$]")
func.plotE(func.f_3dx, func.f_3, -2,2,0.03225806451612903, "$f_3(x)$ feil i intervallet [-2,2]")
func.plotE(func.f_4dx, func.f_4, 0,10,0.25, "$f_4(x)$ feil i intervallet [0,10]")

func.plotComparison(func.f_1, func.f_1dx, 0.0, 2, 0.00014283673760891302 , "$f'_1(x)$ og $g(x)$ i intervallet [0,2]")
func.plotComparison(func.f_2,func.f_2dx, 0, 2*np.pi,0.002824858757062147, "$f'_2(x)$ og $g(x)$ i intervallet [0,2$\pi$]")
func.plotComparison(func.f_3,func.f_3dx, -2,2,0.03225806451612903, "$f'_3(x)$ og $g(x)$ i intervallet [-2,2]")
func.plotComparison(func.f_4, func.f_4dx, 0,10,0.25, "$f'_4(x)$ og $g(x)$ i intervallet [0,10]")