import csv
import numpy as np
import decimal
import sys
import matplotlib.pyplot as plt
import matplotlib.transforms as pt

def f_1(x):
    return 7 * np.power(x, 2) - 8*x + 1

def f_1dx(x):
    return 14*x - 8

def f_2(x):
    return np.sin(x)

def f_2dx(x):
    return np.cos(x)

def f_3(x):
    return (1 - x)/(np.power(x+3, 2))

def f_3dx(x):
    return (x - 5)/np.power(x+3, 3)

def f_4(x):
    return np.sqrt(1 + np.power(x, 2))

def f_4dx(x):
    return x/np.sqrt(1+np.power(x,2))

def g(func, x, delta):
    return (func(x + delta) - func(x))/delta

def E(func_dx, func, x, delta):
    return np.abs(func_dx(x) - g(func, x, delta))

def find_error_delta(func_dx, func, x):
    for i in range(1,1000):
        delta = 1/i
        candidate = E(func_dx, func, x, delta)
        if candidate <= 0.001:
            return delta

def plotE(func_dx, func, a, b, delta, title):

    x = np.linspace(a,b, num=1000)
    y = [ E(func_dx, func, t, delta) for t in x ]
    #y = [ E(func_dx, func, t, find_error_delta(func_dx, func, t)) for t in x ]

    plt.plot(x,y)
    plt.ylabel("E(x)")
    plt.xlabel("x")
    plt.title(title)
    plt.grid()
    plt.savefig('E_'+func.__name__+'.pdf', bbox_inches='tight', transparent=False)
    return plt.show()

def plotComparison(func, func_dx, a,b,delta,title):
    x = np.linspace(a,b, num=1000)
    y = [g(func,t,delta) for t in x]
    ydx = [func_dx(t) for t in x]
    
    ddx, = plt.plot(x,ydx)
    apr, = plt.plot(x,y, linestyle=(0, (1, 1)))

    plt.legend([apr,ddx],["$g(x)$", "$\\frac{d}{dx}$"])
    plt.ylabel("f(x)")
    plt.xlabel("x")
    plt.title(title)
    plt.grid()
    plt.savefig(func.__name__+'.pdf', bbox_inches='tight', transparent=False)
    return plt.show()

if len(sys.argv) == 2 and sys.argv[1] == "print":
    print("f(x): f(1)=\t", f_1(1.0), ",\t\t\terror < 0.001 E(x)=>x=", find_error_delta(f_1dx,f_1, 1.0))
    print("g(x): g(pi/4)=\t", f_2(np.pi/4.0), ",\terror < 0.001 E(x)=>x=", find_error_delta(f_2dx,f_2, np.pi/4))
    print("h(x): h(1)=\t", f_3(1.0), ",\t\t\terror < 0.001 E(x)=>x=", find_error_delta(f_3dx,f_3, 1.0))
    print("k(x): k(5)=\t", f_4(5.0), ",\terror < 0.001 E(x)=>x=", find_error_delta(f_4dx,f_4, 5.0))

if len(sys.argv) == 2 and sys.argv[1] == "test":
    v1 = 1.0
    v2 = np.pi/4
    v3 = 1.0
    v4 = 5.0
    print(f_1dx(v1), " - ", g(f_1,v1,find_error_delta(f_1dx,f_1, v1)), " - ", find_error_delta(f_1dx,f_1, v1) ,"\n")
    print(f_2dx(v2), " - ", g(f_2,v2,find_error_delta(f_2dx,f_2, v2)), " - ", find_error_delta(f_2dx,f_2, v2) ,"\n")
    print(f_3dx(v3), " - ", g(f_3,v3,find_error_delta(f_3dx,f_3, v3)), " - ", find_error_delta(f_3dx,f_3, v3) ,"\n")
    print(f_4dx(v4), " - ", g(f_4,v4,find_error_delta(f_4dx,f_4, v4)), " - ", find_error_delta(f_4dx,f_4, v4) ,"\n")

#if len(sys.argv) == 2 and sys.argv[1] == "export":
#    with open("export.csv", 'w', newline='') as file:
#        print("exporting...")
#        writer = csv.writer(file)
#        writer.writerow(data)